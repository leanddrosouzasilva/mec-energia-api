import pytest
import datetime
from django.test import TestCase
from universities.models import ConsumerUnit, Contract

class RemoveConsumerUnitAndContractTest(TestCase):
    
    def setUp(self):
        date = '2023-01-01' 
        date = datetime.datetime.strptime(date, '%Y-%m-%d')
        
        self.consumer_unit = ConsumerUnit.objects.create(
            university_id = 1,
            name = 'Test Consumer Unit',
            code = 'TCU',
            is_active = True,
        )
        
        self.contract = Contract.objects.create(
            consumer_unit = self.consumer_unit,
            distributor_id = 1,
            start_date = date,
            tariff_flag = 'Test Tariff Flag',
            supply_voltage = 2.3,
            peak_contracted_demand_in_kw = 100,
            off_peak_contracted_demand_in_kw = 50
        )
    
    def test_remove_consumer_unit_and_contract(self):
        result = ConsumerUnit.remove_consumer_unit_and_contract(self.consumer_unit.id, self.contract.id)
        
        self.assertEqual(result, 'Consumer unit and contract removed sucessfully')
        with self.assertRaises(ConsumerUnit.DoesNotExist):
            ConsumerUnit.objects.get(id=self.consumer_unit.id)
        with self.assertRaises(Contract.DoesNotExist):
            Contract.objects.get(id=self.contract.id)