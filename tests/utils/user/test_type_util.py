import pytest
from utils.user.user_type_util import UserType
from users import models

valid_user_type_as_params = pytest.mark.parametrize('user_type, user_model', [
    ('super_user', models.CustomUser),
    ('university_admin', models.UniversityUser),
    ('university_user', models.UniversityUser)
])

invalid_user_type_as_params = pytest.mark.parametrize('user_type, user_model', [
    (None, None),
])

sut = UserType.is_valid_user_type

@valid_user_type_as_params
def test_accepts_valid_user_type(user_type, user_model):
    assert sut(user_type, user_model) == user_type

@invalid_user_type_as_params
def test_raises_exception_for_invalid_user_type(user_type, user_model):
    with pytest.raises(Exception, match=r'User type .* does not exist'):
        sut(user_type, user_model)


